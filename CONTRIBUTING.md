# Feel free to contribute to this project!

However, before you do, make sure to follow these simple guidelines:
- Read our [Software Quality Management Plan](Software-Quality-Management-Plan),
especially the style guide and version control sections.
- Read our [Issue Tracking Guidelines](Issue-Tracking-Guidelines) before 
responding to an issue.
- Don't be a jerk to other contributor (This may be elaborated on later).