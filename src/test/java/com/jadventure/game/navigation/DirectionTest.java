package com.jadventure.game.navigation;

import static org.junit.Assert.*;
import org.junit.Test;

public class DirectionTest {

    @Test
    public void testGetDirection() {

    	String descN = "To the North";
    	String descS = "To the South";
    	String descW = "To the West";
    	String descE = "To the East";
    	String descU = "Heading up";
    	String descD = "Heading down";
        assertEquals(descN, Direction.NORTH.getDescription());
        assertEquals(descS, Direction.SOUTH.getDescription());
        assertEquals(descW, Direction.WEST.getDescription());
        assertEquals(descE, Direction.EAST.getDescription());
        assertEquals(descU, Direction.UP.getDescription());
        assertEquals(descD, Direction.DOWN.getDescription());
    }
}