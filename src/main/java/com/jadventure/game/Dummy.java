public class Dummy {

	public static int dummyNum() {
		double num = Math.random() * 10;
		int number = ((int) num) + 1;
		return number;
	}

	public static void dummy() {
		int x = dummyNum();
		for(int i = 0; i < x; i++) {
			System.out.println("I am a Dummy function! The Dummy number is " + i);
		}
	}
	
	public static void main(String[] args) {
		System.out.println("I am a Dummy class!");
		dummy();
	}
}