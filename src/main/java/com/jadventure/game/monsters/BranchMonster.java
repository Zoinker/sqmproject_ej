package com.jadventure.game.monsters;

/*
 * A silly monster made of branches
 */
public class BranchMonster extends Monster {
    public BranchMonster(int playerLevel){
        this.monsterType = "Branch Monster";
        this.setHealthMax(1 + (int) Math.pow(playerLevel, 1));
        this.setHealth(1 + (int) Math.pow(playerLevel, 1));
        this.setArmour(0);
        this.setDamage(1);
        this.setCritChance(0.01);
                this.setIntelligence(1);
                this.setStealth(1);
                this.setDexterity(10);        
                this.setXPGain(1);
        this.setGold(1);
                addRandomItems(playerLevel, "arhl1");
    }
}
