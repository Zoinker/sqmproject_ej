package com.jadventure.game.monsters;

/*
 * Humongous boss monster
 */
public class Gigataurus extends Monster {
    public Gigataurus(int playerLevel){
        this.monsterType = "Gigataurus";
        this.setHealthMax(666 + (int) Math.pow(playerLevel, 10));
        this.setHealth(666 + (int) Math.pow(playerLevel, 10));
        this.setArmour(10);
        this.setDamage(42 + Math.pow(playerLevel, 5));
        this.setCritChance(0.1);
                this.setIntelligence(1);
                this.setStealth(1);
                this.setDexterity(3);        
                this.setXPGain(50 + playerLevel * 50);
        this.setGold(playerLevel * 100);
                addRandomItems(playerLevel, "arhl1");
    }
}
